--[[ This snippet defines a tween function that's always able to be called with any element and property. ]]--

--// Get the TweenService service.
local TweenService = game:GetService("TweenService")

--// Create the function 'Tween'.
local function Tween(object, information, goal)

    --// Check if all arguments were provided for the function.
    if (object) and (information) and (goal) then

        --// Get the applied arguments and create a tween, then play it instantly without delay.
        TweenService:Create(object, information, goal):Play()

    else

        --// If not all arguments were provided, warn the developer instead of halting the script.
        warn("Not all arguments provided")

    end

end
